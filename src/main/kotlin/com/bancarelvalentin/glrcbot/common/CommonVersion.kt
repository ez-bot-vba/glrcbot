package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.logger.LogCategoriesEnum
import com.bancarelvalentin.ezbot.logger.Logger
import org.apache.commons.io.IOUtils


class CommonVersion {

    private val version = try {
        val resourceAsStream = this.javaClass.getResourceAsStream("/version.txt")
        IOUtils.readLines(resourceAsStream).joinToString { it as String }
    } catch (e: Exception) {
        Logger(this.javaClass).error("Can't get version", LogCategoriesEnum.INITIAL_STARTUP, e)
        "[ERROR]"
    }

    override fun toString(): String {
        return version
    }
}