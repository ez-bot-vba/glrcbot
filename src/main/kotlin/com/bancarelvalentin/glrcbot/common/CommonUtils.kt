package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.i18n.I18nDictionary
import com.bancarelvalentin.ezbot.i18n.Localizator
import com.bancarelvalentin.glrcbot.common.process.common.background.deletetempchannelondisconnectbot.DeleteTempChannelOndisconnectBot
import com.bancarelvalentin.glrcbot.common.process.common.background.dmtogifbot.DmToGifBot
import com.bancarelvalentin.glrcbot.common.process.common.background.logdmbot.LogDmsBot
import com.bancarelvalentin.glrcbot.common.process.common.background.mentionreplybot.MentionReplyBot
import com.bancarelvalentin.glrcbot.common.process.common.background.onbastardmsgbot.OnBastardMsgBot
import com.bancarelvalentin.glrcbot.common.process.common.background.onwelcomebot.OnWelcomeBot
import com.bancarelvalentin.glrcbot.common.process.common.command.addbirthday.AddBirthdayTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.gameinvite.GameInviteTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.gamesupport.GameSupportAddTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.gamesupport.GameSupportRemoveTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.react.ReactBotTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.reply.ReplyBotTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.talk.TalkBotTextCommand
import com.bancarelvalentin.glrcbot.common.process.common.command.zeventclipssaverbot.ZeventClipsSaverForcecommand
import com.bancarelvalentin.glrcbot.common.process.unique.background.birthday.BirthdayBot
import com.bancarelvalentin.glrcbot.common.process.unique.background.gamegrantsbot.GameGrantsBot
import com.bancarelvalentin.glrcbot.common.process.unique.background.genericinvitebot.GenericInviteBot
import java.util.Calendar


class CommonUtils {
    companion object {
        
        @Suppress("unused")
        fun addAllProcessToOrchestrator() {
            if ((ConfigHandler.config as CommonConfig).identity.isTheSuperiorOne) {
                Orchestrator.add(GameGrantsBot())
                Orchestrator.add(GenericInviteBot())
                Orchestrator.add(DeleteTempChannelOndisconnectBot())
            }
            
            Orchestrator.add(AddBirthdayTextCommand())
            Orchestrator.add(GameInviteTextCommand())
            Orchestrator.add(GameSupportAddTextCommand())
            Orchestrator.add(TalkBotTextCommand())
            Orchestrator.add(ReplyBotTextCommand())
            Orchestrator.add(ReactBotTextCommand())
            Orchestrator.add(GameSupportRemoveTextCommand())
            Orchestrator.add(ZeventClipsSaverForcecommand())
            
            Orchestrator.add(BirthdayBot())
            Orchestrator.add(MentionReplyBot())
            Orchestrator.add(OnBastardMsgBot())
            Orchestrator.add(OnWelcomeBot())
            Orchestrator.add(DmToGifBot())
            Orchestrator.add(LogDmsBot())
            //TODO Orchestrator.add(ZeventClipsSaverBot())
        }
        
        fun isMyTurn(): Boolean {
            val minute = Calendar.getInstance().get(Calendar.MINUTE)
            val index = minute % IDENTITY.values().size
            val isMyTurn = (ConfigHandler.config as CommonConfig).identity == IDENTITY.values()[index]
            return EnvConfig.DEV || isMyTurn
        }
        
        fun localize(localizeEnum: I18nDictionary, vararg args: Any): String {
            return Localizator.get(localizeEnum, null, *args)
        }
    }
}