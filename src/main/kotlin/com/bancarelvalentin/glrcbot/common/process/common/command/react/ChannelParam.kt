package com.bancarelvalentin.glrcbot.common.process.common.command.react

import com.bancarelvalentin.ezbot.process.command.param.ChannelCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class ChannelParam : ChannelCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__REACT__CHANNEL__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__REACT__CHANNEL__DESC)
}