package com.bancarelvalentin.glrcbot.common.process.common.command.react

import com.bancarelvalentin.ezbot.process.command.Command
import com.bancarelvalentin.ezbot.process.command.param.CommandParam
import com.bancarelvalentin.ezbot.process.command.request.CommandRequest
import com.bancarelvalentin.ezbot.process.command.response.CommandResponse
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import java.util.function.BiConsumer

class ReactBotTextCommand : Command() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__REACT__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__REACT__DESC)
    override val sample = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD__REACT__SAMPLE)
    
    override val whitelistChannelIds = arrayOf(HardCodedValues.CHANNEL__BOT_COMMANDS_ADMIN)
    override val whitelistRoleIds = arrayOf(HardCodedValues.ROLE__MAITRE_EQUIPAGE)
    override val patterns = arrayOf("react")
    override val paramsClasses: Array<Class<out CommandParam<out Any?>>> = arrayOf(ChannelParam::class.java, MessageIdParam::class.java, EmoteParam::class.java)
    
    override val logic = BiConsumer { request: CommandRequest, _: CommandResponse ->
        val channel = request.getParamChannel(0)!!
        val messageId = request.getParamInt(1)!!
        val emote = request.getParamString(2)!!
        channel.retrieveMessageById(messageId.toString()).queue {
            it.addReaction(emote).queue()
        }
    }
}
