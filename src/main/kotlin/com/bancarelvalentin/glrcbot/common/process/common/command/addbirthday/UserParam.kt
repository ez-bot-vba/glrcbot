package com.bancarelvalentin.glrcbot.common.process.common.command.addbirthday

import com.bancarelvalentin.ezbot.process.command.param.UserCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class UserParam : UserCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__USER__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__ADD_BIRTHDAY__USER__DESC)
}
