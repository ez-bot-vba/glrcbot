package com.bancarelvalentin.glrcbot.common.process.common.command.react

import com.bancarelvalentin.ezbot.process.command.param.IntCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class MessageIdParam : IntCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__REACT__MESSAGE_ID__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__REACT__MESSAGE_ID__DESC)
}