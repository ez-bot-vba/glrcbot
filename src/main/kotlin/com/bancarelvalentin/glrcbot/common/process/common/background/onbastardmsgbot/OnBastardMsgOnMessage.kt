package com.bancarelvalentin.glrcbot.common.process.common.background.onbastardmsgbot

import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSpecificSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.process.common.GlrcTurnBasedEventListener
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class OnBastardMsgOnMessage(process: Process) : GlrcTurnBasedEventListener(process) {
    
    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.author.isBot) return
        
        if (event.message.contentRaw.contains(Regex("b([aâ])tard?([^\\w]|\$)")))
            event.message.reply(CommonUtils.localize(GlrcSpecificSimpleLocalizeEnum.PROCESS__ON_BASTARD__RESPONSE)).queue()
    }
}

