package com.bancarelvalentin.glrcbot.common.process.common

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.CommonUtils
import net.dv8tion.jda.api.events.DisconnectEvent
import net.dv8tion.jda.api.events.ExceptionEvent
import net.dv8tion.jda.api.events.GatewayPingEvent
import net.dv8tion.jda.api.events.GenericEvent
import net.dv8tion.jda.api.events.RawGatewayEvent
import net.dv8tion.jda.api.events.ReadyEvent
import net.dv8tion.jda.api.events.ReconnectedEvent
import net.dv8tion.jda.api.events.ResumedEvent
import net.dv8tion.jda.api.events.ShutdownEvent
import net.dv8tion.jda.api.events.StatusChangeEvent
import net.dv8tion.jda.api.events.UpdateEvent
import net.dv8tion.jda.api.events.application.ApplicationCommandCreateEvent
import net.dv8tion.jda.api.events.application.ApplicationCommandDeleteEvent
import net.dv8tion.jda.api.events.application.ApplicationCommandUpdateEvent
import net.dv8tion.jda.api.events.application.GenericApplicationCommandEvent
import net.dv8tion.jda.api.events.channel.ChannelCreateEvent
import net.dv8tion.jda.api.events.channel.ChannelDeleteEvent
import net.dv8tion.jda.api.events.channel.GenericChannelEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateArchiveTimestampEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateArchivedEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateAutoArchiveDurationEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateBitrateEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateInvitableEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateLockedEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateNSFWEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateNameEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateParentEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdatePositionEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateRegionEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateSlowmodeEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateTopicEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateTypeEvent
import net.dv8tion.jda.api.events.channel.update.ChannelUpdateUserLimitEvent
import net.dv8tion.jda.api.events.channel.update.GenericChannelUpdateEvent
import net.dv8tion.jda.api.events.emote.EmoteAddedEvent
import net.dv8tion.jda.api.events.emote.EmoteRemovedEvent
import net.dv8tion.jda.api.events.emote.GenericEmoteEvent
import net.dv8tion.jda.api.events.emote.update.EmoteUpdateNameEvent
import net.dv8tion.jda.api.events.emote.update.EmoteUpdateRolesEvent
import net.dv8tion.jda.api.events.emote.update.GenericEmoteUpdateEvent
import net.dv8tion.jda.api.events.guild.GenericGuildEvent
import net.dv8tion.jda.api.events.guild.GuildAvailableEvent
import net.dv8tion.jda.api.events.guild.GuildBanEvent
import net.dv8tion.jda.api.events.guild.GuildJoinEvent
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent
import net.dv8tion.jda.api.events.guild.GuildReadyEvent
import net.dv8tion.jda.api.events.guild.GuildTimeoutEvent
import net.dv8tion.jda.api.events.guild.GuildUnavailableEvent
import net.dv8tion.jda.api.events.guild.GuildUnbanEvent
import net.dv8tion.jda.api.events.guild.UnavailableGuildJoinedEvent
import net.dv8tion.jda.api.events.guild.UnavailableGuildLeaveEvent
import net.dv8tion.jda.api.events.guild.invite.GenericGuildInviteEvent
import net.dv8tion.jda.api.events.guild.invite.GuildInviteCreateEvent
import net.dv8tion.jda.api.events.guild.invite.GuildInviteDeleteEvent
import net.dv8tion.jda.api.events.guild.member.GenericGuildMemberEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRemoveEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleRemoveEvent
import net.dv8tion.jda.api.events.guild.member.GuildMemberUpdateEvent
import net.dv8tion.jda.api.events.guild.member.update.GenericGuildMemberUpdateEvent
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateAvatarEvent
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateBoostTimeEvent
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdatePendingEvent
import net.dv8tion.jda.api.events.guild.override.GenericPermissionOverrideEvent
import net.dv8tion.jda.api.events.guild.override.PermissionOverrideCreateEvent
import net.dv8tion.jda.api.events.guild.override.PermissionOverrideDeleteEvent
import net.dv8tion.jda.api.events.guild.override.PermissionOverrideUpdateEvent
import net.dv8tion.jda.api.events.guild.update.GenericGuildUpdateEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateAfkChannelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateAfkTimeoutEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateBannerEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateBoostCountEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateBoostTierEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateCommunityUpdatesChannelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateDescriptionEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateExplicitContentLevelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateFeaturesEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateIconEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateLocaleEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateMFALevelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateMaxMembersEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateMaxPresencesEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNSFWLevelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNameEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateNotificationLevelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateOwnerEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateRulesChannelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateSplashEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateSystemChannelEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateVanityCodeEvent
import net.dv8tion.jda.api.events.guild.update.GuildUpdateVerificationLevelEvent
import net.dv8tion.jda.api.events.guild.voice.GenericGuildVoiceEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceDeafenEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceGuildDeafenEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceGuildMuteEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMuteEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceRequestToSpeakEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceSelfDeafenEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceSelfMuteEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceStreamEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceSuppressEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceVideoEvent
import net.dv8tion.jda.api.events.http.HttpRequestEvent
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent
import net.dv8tion.jda.api.events.interaction.GenericComponentInteractionCreateEvent
import net.dv8tion.jda.api.events.interaction.GenericInteractionCreateEvent
import net.dv8tion.jda.api.events.interaction.SelectionMenuEvent
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent
import net.dv8tion.jda.api.events.message.GenericMessageEvent
import net.dv8tion.jda.api.events.message.MessageBulkDeleteEvent
import net.dv8tion.jda.api.events.message.MessageDeleteEvent
import net.dv8tion.jda.api.events.message.MessageEmbedEvent
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.events.message.MessageUpdateEvent
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveAllEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEmoteEvent
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent
import net.dv8tion.jda.api.events.role.GenericRoleEvent
import net.dv8tion.jda.api.events.role.RoleCreateEvent
import net.dv8tion.jda.api.events.role.RoleDeleteEvent
import net.dv8tion.jda.api.events.role.update.GenericRoleUpdateEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateColorEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateHoistedEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateIconEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateMentionableEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdateNameEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdatePermissionsEvent
import net.dv8tion.jda.api.events.role.update.RoleUpdatePositionEvent
import net.dv8tion.jda.api.events.self.GenericSelfUpdateEvent
import net.dv8tion.jda.api.events.self.SelfUpdateAvatarEvent
import net.dv8tion.jda.api.events.self.SelfUpdateMFAEvent
import net.dv8tion.jda.api.events.self.SelfUpdateNameEvent
import net.dv8tion.jda.api.events.self.SelfUpdateVerifiedEvent
import net.dv8tion.jda.api.events.stage.GenericStageInstanceEvent
import net.dv8tion.jda.api.events.stage.StageInstanceCreateEvent
import net.dv8tion.jda.api.events.stage.StageInstanceDeleteEvent
import net.dv8tion.jda.api.events.stage.update.GenericStageInstanceUpdateEvent
import net.dv8tion.jda.api.events.stage.update.StageInstanceUpdatePrivacyLevelEvent
import net.dv8tion.jda.api.events.stage.update.StageInstanceUpdateTopicEvent
import net.dv8tion.jda.api.events.thread.GenericThreadEvent
import net.dv8tion.jda.api.events.thread.ThreadHiddenEvent
import net.dv8tion.jda.api.events.thread.ThreadRevealedEvent
import net.dv8tion.jda.api.events.thread.member.GenericThreadMemberEvent
import net.dv8tion.jda.api.events.thread.member.ThreadMemberJoinEvent
import net.dv8tion.jda.api.events.thread.member.ThreadMemberLeaveEvent
import net.dv8tion.jda.api.events.user.GenericUserEvent
import net.dv8tion.jda.api.events.user.UserActivityEndEvent
import net.dv8tion.jda.api.events.user.UserActivityStartEvent
import net.dv8tion.jda.api.events.user.UserTypingEvent
import net.dv8tion.jda.api.events.user.update.GenericUserPresenceEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateActivitiesEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateActivityOrderEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateAvatarEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateDiscriminatorEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateFlagsEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateNameEvent
import net.dv8tion.jda.api.events.user.update.UserUpdateOnlineStatusEvent

abstract class GlrcTurnBasedEventListener(process: Process) : EventListener(process) {
    override fun onApplicationCommandCreate(event: ApplicationCommandCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onApplicationCommandCreate(event)
    }
    
    override fun onApplicationCommandDelete(event: ApplicationCommandDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onApplicationCommandDelete(event)
    }
    
    override fun onApplicationCommandUpdate(event: ApplicationCommandUpdateEvent) {
        if (CommonUtils.isMyTurn())
            super.onApplicationCommandUpdate(event)
    }
    
    override fun onButtonClick(event: ButtonClickEvent) {
        if (CommonUtils.isMyTurn())
            super.onButtonClick(event)
    }
    
    override fun onChannelCreate(event: ChannelCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelCreate(event)
    }
    
    override fun onChannelDelete(event: ChannelDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelDelete(event)
    }
    
    override fun onChannelUpdateArchiveTimestamp(event: ChannelUpdateArchiveTimestampEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateArchiveTimestamp(event)
    }
    
    override fun onChannelUpdateArchived(event: ChannelUpdateArchivedEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateArchived(event)
    }
    
    override fun onChannelUpdateAutoArchiveDuration(event: ChannelUpdateAutoArchiveDurationEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateAutoArchiveDuration(event)
    }
    
    override fun onChannelUpdateBitrate(event: ChannelUpdateBitrateEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateBitrate(event)
    }
    
    override fun onChannelUpdateInvitable(event: ChannelUpdateInvitableEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateInvitable(event)
    }
    
    override fun onChannelUpdateLocked(event: ChannelUpdateLockedEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateLocked(event)
    }
    
    override fun onChannelUpdateNSFW(event: ChannelUpdateNSFWEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateNSFW(event)
    }
    
    override fun onChannelUpdateName(event: ChannelUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateName(event)
    }
    
    override fun onChannelUpdateParent(event: ChannelUpdateParentEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateParent(event)
    }
    
    override fun onChannelUpdatePosition(event: ChannelUpdatePositionEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdatePosition(event)
    }
    
    override fun onChannelUpdateRegion(event: ChannelUpdateRegionEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateRegion(event)
    }
    
    override fun onChannelUpdateSlowmode(event: ChannelUpdateSlowmodeEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateSlowmode(event)
    }
    
    override fun onChannelUpdateTopic(event: ChannelUpdateTopicEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateTopic(event)
    }
    
    override fun onChannelUpdateType(event: ChannelUpdateTypeEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateType(event)
    }
    
    override fun onChannelUpdateUserLimit(event: ChannelUpdateUserLimitEvent) {
        if (CommonUtils.isMyTurn())
            super.onChannelUpdateUserLimit(event)
    }
    
    override fun onDisconnect(event: DisconnectEvent) {
        if (CommonUtils.isMyTurn())
            super.onDisconnect(event)
    }
    
    override fun onEmoteAdded(event: EmoteAddedEvent) {
        if (CommonUtils.isMyTurn())
            super.onEmoteAdded(event)
    }
    
    override fun onEmoteRemoved(event: EmoteRemovedEvent) {
        if (CommonUtils.isMyTurn())
            super.onEmoteRemoved(event)
    }
    
    override fun onEmoteUpdateName(event: EmoteUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onEmoteUpdateName(event)
    }
    
    override fun onEmoteUpdateRoles(event: EmoteUpdateRolesEvent) {
        if (CommonUtils.isMyTurn())
            super.onEmoteUpdateRoles(event)
    }
    
    override fun onException(event: ExceptionEvent) {
        if (CommonUtils.isMyTurn())
            super.onException(event)
    }
    
    override fun onGatewayPing(event: GatewayPingEvent) {
        if (CommonUtils.isMyTurn())
            super.onGatewayPing(event)
    }
    
    override fun onGenericApplicationCommand(event: GenericApplicationCommandEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericApplicationCommand(event)
    }
    
    override fun onGenericChannel(event: GenericChannelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericChannel(event)
    }
    
    override fun onGenericChannelUpdate(event: GenericChannelUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericChannelUpdate(event)
    }
    
    override fun onGenericComponentInteractionCreate(event: GenericComponentInteractionCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericComponentInteractionCreate(event)
    }
    
    override fun onGenericEmote(event: GenericEmoteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericEmote(event)
    }
    
    override fun onGenericEmoteUpdate(event: GenericEmoteUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericEmoteUpdate(event)
    }
    
    override fun onGenericEvent(event: GenericEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericEvent(event)
    }
    
    override fun onGenericGuild(event: GenericGuildEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuild(event)
    }
    
    override fun onGenericGuildInvite(event: GenericGuildInviteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuildInvite(event)
    }
    
    override fun onGenericGuildMember(event: GenericGuildMemberEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuildMember(event)
    }
    
    override fun onGenericGuildMemberUpdate(event: GenericGuildMemberUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuildMemberUpdate(event)
    }
    
    override fun onGenericGuildUpdate(event: GenericGuildUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuildUpdate(event)
    }
    
    override fun onGenericGuildVoice(event: GenericGuildVoiceEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericGuildVoice(event)
    }
    
    override fun onGenericInteractionCreate(event: GenericInteractionCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericInteractionCreate(event)
    }
    
    override fun onGenericMessage(event: GenericMessageEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericMessage(event)
    }
    
    override fun onGenericMessageReaction(event: GenericMessageReactionEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericMessageReaction(event)
    }
    
    override fun onGenericPermissionOverride(event: GenericPermissionOverrideEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericPermissionOverride(event)
    }
    
    override fun onGenericRole(event: GenericRoleEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericRole(event)
    }
    
    override fun onGenericRoleUpdate(event: GenericRoleUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericRoleUpdate(event)
    }
    
    override fun onGenericSelfUpdate(event: GenericSelfUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericSelfUpdate(event)
    }
    
    override fun onGenericStageInstance(event: GenericStageInstanceEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericStageInstance(event)
    }
    
    override fun onGenericStageInstanceUpdate(event: GenericStageInstanceUpdateEvent<*>) {
        if (CommonUtils.isMyTurn())
            super.onGenericStageInstanceUpdate(event)
    }
    
    override fun onGenericThread(event: GenericThreadEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericThread(event)
    }
    
    override fun onGenericThreadMember(event: GenericThreadMemberEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericThreadMember(event)
    }
    
    override fun onGenericUpdate(event: UpdateEvent<*, *>) {
        if (CommonUtils.isMyTurn())
            super.onGenericUpdate(event)
    }
    
    override fun onGenericUser(event: GenericUserEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericUser(event)
    }
    
    override fun onGenericUserPresence(event: GenericUserPresenceEvent) {
        if (CommonUtils.isMyTurn())
            super.onGenericUserPresence(event)
    }
    
    override fun onGuildAvailable(event: GuildAvailableEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildAvailable(event)
    }
    
    override fun onGuildBan(event: GuildBanEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildBan(event)
    }
    
    override fun onGuildInviteCreate(event: GuildInviteCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildInviteCreate(event)
    }
    
    override fun onGuildInviteDelete(event: GuildInviteDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildInviteDelete(event)
    }
    
    override fun onGuildJoin(event: GuildJoinEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildJoin(event)
    }
    
    override fun onGuildLeave(event: GuildLeaveEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildLeave(event)
    }
    
    override fun onGuildMemberJoin(event: GuildMemberJoinEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberJoin(event)
    }
    
    override fun onGuildMemberRemove(event: GuildMemberRemoveEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberRemove(event)
    }
    
    override fun onGuildMemberRoleAdd(event: GuildMemberRoleAddEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberRoleAdd(event)
    }
    
    override fun onGuildMemberRoleRemove(event: GuildMemberRoleRemoveEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberRoleRemove(event)
    }
    
    override fun onGuildMemberUpdate(event: GuildMemberUpdateEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberUpdate(event)
    }
    
    override fun onGuildMemberUpdateAvatar(event: GuildMemberUpdateAvatarEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberUpdateAvatar(event)
    }
    
    override fun onGuildMemberUpdateBoostTime(event: GuildMemberUpdateBoostTimeEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberUpdateBoostTime(event)
    }
    
    override fun onGuildMemberUpdateNickname(event: GuildMemberUpdateNicknameEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberUpdateNickname(event)
    }
    
    override fun onGuildMemberUpdatePending(event: GuildMemberUpdatePendingEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildMemberUpdatePending(event)
    }
    
    override fun onGuildReady(event: GuildReadyEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildReady(event)
    }
    
    override fun onGuildTimeout(event: GuildTimeoutEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildTimeout(event)
    }
    
    override fun onGuildUnavailable(event: GuildUnavailableEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUnavailable(event)
    }
    
    override fun onGuildUnban(event: GuildUnbanEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUnban(event)
    }
    
    override fun onGuildUpdateAfkChannel(event: GuildUpdateAfkChannelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateAfkChannel(event)
    }
    
    override fun onGuildUpdateAfkTimeout(event: GuildUpdateAfkTimeoutEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateAfkTimeout(event)
    }
    
    override fun onGuildUpdateBanner(event: GuildUpdateBannerEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateBanner(event)
    }
    
    override fun onGuildUpdateBoostCount(event: GuildUpdateBoostCountEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateBoostCount(event)
    }
    
    override fun onGuildUpdateBoostTier(event: GuildUpdateBoostTierEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateBoostTier(event)
    }
    
    override fun onGuildUpdateCommunityUpdatesChannel(event: GuildUpdateCommunityUpdatesChannelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateCommunityUpdatesChannel(event)
    }
    
    override fun onGuildUpdateDescription(event: GuildUpdateDescriptionEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateDescription(event)
    }
    
    override fun onGuildUpdateExplicitContentLevel(event: GuildUpdateExplicitContentLevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateExplicitContentLevel(event)
    }
    
    override fun onGuildUpdateFeatures(event: GuildUpdateFeaturesEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateFeatures(event)
    }
    
    override fun onGuildUpdateIcon(event: GuildUpdateIconEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateIcon(event)
    }
    
    override fun onGuildUpdateLocale(event: GuildUpdateLocaleEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateLocale(event)
    }
    
    override fun onGuildUpdateMFALevel(event: GuildUpdateMFALevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateMFALevel(event)
    }
    
    override fun onGuildUpdateMaxMembers(event: GuildUpdateMaxMembersEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateMaxMembers(event)
    }
    
    override fun onGuildUpdateMaxPresences(event: GuildUpdateMaxPresencesEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateMaxPresences(event)
    }
    
    override fun onGuildUpdateNSFWLevel(event: GuildUpdateNSFWLevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateNSFWLevel(event)
    }
    
    override fun onGuildUpdateName(event: GuildUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateName(event)
    }
    
    override fun onGuildUpdateNotificationLevel(event: GuildUpdateNotificationLevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateNotificationLevel(event)
    }
    
    override fun onGuildUpdateOwner(event: GuildUpdateOwnerEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateOwner(event)
    }
    
    override fun onGuildUpdateRulesChannel(event: GuildUpdateRulesChannelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateRulesChannel(event)
    }
    
    override fun onGuildUpdateSplash(event: GuildUpdateSplashEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateSplash(event)
    }
    
    override fun onGuildUpdateSystemChannel(event: GuildUpdateSystemChannelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateSystemChannel(event)
    }
    
    override fun onGuildUpdateVanityCode(event: GuildUpdateVanityCodeEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateVanityCode(event)
    }
    
    override fun onGuildUpdateVerificationLevel(event: GuildUpdateVerificationLevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildUpdateVerificationLevel(event)
    }
    
    override fun onGuildVoiceDeafen(event: GuildVoiceDeafenEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceDeafen(event)
    }
    
    override fun onGuildVoiceGuildDeafen(event: GuildVoiceGuildDeafenEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceGuildDeafen(event)
    }
    
    override fun onGuildVoiceGuildMute(event: GuildVoiceGuildMuteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceGuildMute(event)
    }
    
    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceJoin(event)
    }
    
    override fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceLeave(event)
    }
    
    override fun onGuildVoiceMove(event: GuildVoiceMoveEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceMove(event)
    }
    
    override fun onGuildVoiceMute(event: GuildVoiceMuteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceMute(event)
    }
    
    override fun onGuildVoiceRequestToSpeak(event: GuildVoiceRequestToSpeakEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceRequestToSpeak(event)
    }
    
    override fun onGuildVoiceSelfDeafen(event: GuildVoiceSelfDeafenEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceSelfDeafen(event)
    }
    
    override fun onGuildVoiceSelfMute(event: GuildVoiceSelfMuteEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceSelfMute(event)
    }
    
    override fun onGuildVoiceStream(event: GuildVoiceStreamEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceStream(event)
    }
    
    override fun onGuildVoiceSuppress(event: GuildVoiceSuppressEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceSuppress(event)
    }
    
    override fun onGuildVoiceUpdate(event: GuildVoiceUpdateEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceUpdate(event)
    }
    
    override fun onGuildVoiceVideo(event: GuildVoiceVideoEvent) {
        if (CommonUtils.isMyTurn())
            super.onGuildVoiceVideo(event)
    }
    
    override fun onHttpRequest(event: HttpRequestEvent) {
        if (CommonUtils.isMyTurn())
            super.onHttpRequest(event)
    }
    
    override fun onMessageBulkDelete(event: MessageBulkDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageBulkDelete(event)
    }
    
    override fun onMessageDelete(event: MessageDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageDelete(event)
    }
    
    override fun onMessageEmbed(event: MessageEmbedEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageEmbed(event)
    }
    
    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageReactionAdd(event)
    }
    
    override fun onMessageReactionRemove(event: MessageReactionRemoveEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageReactionRemove(event)
    }
    
    override fun onMessageReactionRemoveAll(event: MessageReactionRemoveAllEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageReactionRemoveAll(event)
    }
    
    override fun onMessageReactionRemoveEmote(event: MessageReactionRemoveEmoteEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageReactionRemoveEmote(event)
    }
    
    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageReceived(event)
    }
    
    override fun onMessageUpdate(event: MessageUpdateEvent) {
        if (CommonUtils.isMyTurn())
            super.onMessageUpdate(event)
    }
    
    override fun onPermissionOverrideCreate(event: PermissionOverrideCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onPermissionOverrideCreate(event)
    }
    
    override fun onPermissionOverrideDelete(event: PermissionOverrideDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onPermissionOverrideDelete(event)
    }
    
    override fun onPermissionOverrideUpdate(event: PermissionOverrideUpdateEvent) {
        if (CommonUtils.isMyTurn())
            super.onPermissionOverrideUpdate(event)
    }
    
    override fun onRawGateway(event: RawGatewayEvent) {
        if (CommonUtils.isMyTurn())
            super.onRawGateway(event)
    }
    
    override fun onReady(event: ReadyEvent) {
        if (CommonUtils.isMyTurn())
            super.onReady(event)
    }
    
    override fun onReconnected(event: ReconnectedEvent) {
        if (CommonUtils.isMyTurn())
            super.onReconnected(event)
    }
    
    override fun onResumed(event: ResumedEvent) {
        if (CommonUtils.isMyTurn())
            super.onResumed(event)
    }
    
    override fun onRoleCreate(event: RoleCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleCreate(event)
    }
    
    override fun onRoleDelete(event: RoleDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleDelete(event)
    }
    
    override fun onRoleUpdateColor(event: RoleUpdateColorEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdateColor(event)
    }
    
    override fun onRoleUpdateHoisted(event: RoleUpdateHoistedEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdateHoisted(event)
    }
    
    override fun onRoleUpdateIcon(event: RoleUpdateIconEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdateIcon(event)
    }
    
    override fun onRoleUpdateMentionable(event: RoleUpdateMentionableEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdateMentionable(event)
    }
    
    override fun onRoleUpdateName(event: RoleUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdateName(event)
    }
    
    override fun onRoleUpdatePermissions(event: RoleUpdatePermissionsEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdatePermissions(event)
    }
    
    override fun onRoleUpdatePosition(event: RoleUpdatePositionEvent) {
        if (CommonUtils.isMyTurn())
            super.onRoleUpdatePosition(event)
    }
    
    override fun onSelectionMenu(event: SelectionMenuEvent) {
        if (CommonUtils.isMyTurn())
            super.onSelectionMenu(event)
    }
    
    override fun onSelfUpdateAvatar(event: SelfUpdateAvatarEvent) {
        if (CommonUtils.isMyTurn())
            super.onSelfUpdateAvatar(event)
    }
    
    override fun onSelfUpdateMFA(event: SelfUpdateMFAEvent) {
        if (CommonUtils.isMyTurn())
            super.onSelfUpdateMFA(event)
    }
    
    override fun onSelfUpdateName(event: SelfUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onSelfUpdateName(event)
    }
    
    override fun onSelfUpdateVerified(event: SelfUpdateVerifiedEvent) {
        if (CommonUtils.isMyTurn())
            super.onSelfUpdateVerified(event)
    }
    
    override fun onShutdown(event: ShutdownEvent) {
        if (CommonUtils.isMyTurn())
            super.onShutdown(event)
    }
    
    override fun onSlashCommand(event: SlashCommandEvent) {
        if (CommonUtils.isMyTurn())
            super.onSlashCommand(event)
    }
    
    override fun onStageInstanceCreate(event: StageInstanceCreateEvent) {
        if (CommonUtils.isMyTurn())
            super.onStageInstanceCreate(event)
    }
    
    override fun onStageInstanceDelete(event: StageInstanceDeleteEvent) {
        if (CommonUtils.isMyTurn())
            super.onStageInstanceDelete(event)
    }
    
    override fun onStageInstanceUpdatePrivacyLevel(event: StageInstanceUpdatePrivacyLevelEvent) {
        if (CommonUtils.isMyTurn())
            super.onStageInstanceUpdatePrivacyLevel(event)
    }
    
    override fun onStageInstanceUpdateTopic(event: StageInstanceUpdateTopicEvent) {
        if (CommonUtils.isMyTurn())
            super.onStageInstanceUpdateTopic(event)
    }
    
    override fun onStatusChange(event: StatusChangeEvent) {
        if (CommonUtils.isMyTurn())
            super.onStatusChange(event)
    }
    
    override fun onThreadHidden(event: ThreadHiddenEvent) {
        if (CommonUtils.isMyTurn())
            super.onThreadHidden(event)
    }
    
    override fun onThreadMemberJoin(event: ThreadMemberJoinEvent) {
        if (CommonUtils.isMyTurn())
            super.onThreadMemberJoin(event)
    }
    
    override fun onThreadMemberLeave(event: ThreadMemberLeaveEvent) {
        if (CommonUtils.isMyTurn())
            super.onThreadMemberLeave(event)
    }
    
    override fun onThreadRevealed(event: ThreadRevealedEvent) {
        if (CommonUtils.isMyTurn())
            super.onThreadRevealed(event)
    }
    
    override fun onUnavailableGuildJoined(event: UnavailableGuildJoinedEvent) {
        if (CommonUtils.isMyTurn())
            super.onUnavailableGuildJoined(event)
    }
    
    override fun onUnavailableGuildLeave(event: UnavailableGuildLeaveEvent) {
        if (CommonUtils.isMyTurn())
            super.onUnavailableGuildLeave(event)
    }
    
    override fun onUserActivityEnd(event: UserActivityEndEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserActivityEnd(event)
    }
    
    override fun onUserActivityStart(event: UserActivityStartEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserActivityStart(event)
    }
    
    override fun onUserTyping(event: UserTypingEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserTyping(event)
    }
    
    override fun onUserUpdateActivities(event: UserUpdateActivitiesEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateActivities(event)
    }
    
    override fun onUserUpdateActivityOrder(event: UserUpdateActivityOrderEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateActivityOrder(event)
    }
    
    override fun onUserUpdateAvatar(event: UserUpdateAvatarEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateAvatar(event)
    }
    
    override fun onUserUpdateDiscriminator(event: UserUpdateDiscriminatorEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateDiscriminator(event)
    }
    
    override fun onUserUpdateFlags(event: UserUpdateFlagsEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateFlags(event)
    }
    
    override fun onUserUpdateName(event: UserUpdateNameEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateName(event)
    }
    
    override fun onUserUpdateOnlineStatus(event: UserUpdateOnlineStatusEvent) {
        if (CommonUtils.isMyTurn())
            super.onUserUpdateOnlineStatus(event)
    }
}