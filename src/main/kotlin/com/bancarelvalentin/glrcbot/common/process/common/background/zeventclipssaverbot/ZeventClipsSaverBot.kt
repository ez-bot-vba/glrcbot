package com.bancarelvalentin.glrcbot.common.process.common.background.zeventclipssaverbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class ZeventClipsSaverBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__EVENT_CLIP_SAVER__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__EVENT_CLIP_SAVER__DESC)
    
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(ZeventClipsSaverBotOnReaction(this))
    }
}