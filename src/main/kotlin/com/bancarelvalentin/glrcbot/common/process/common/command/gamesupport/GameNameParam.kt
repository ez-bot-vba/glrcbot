package com.bancarelvalentin.glrcbot.common.process.common.command.gamesupport

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class GameNameParam : StringCommandParam() {
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__ROLE_NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_SUPPORT_ADD__ROLE_DESC)
}