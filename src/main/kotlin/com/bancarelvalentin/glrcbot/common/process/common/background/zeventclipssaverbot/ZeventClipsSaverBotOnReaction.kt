package com.bancarelvalentin.glrcbot.common.process.common.background.zeventclipssaverbot

import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import com.bancarelvalentin.glrcbot.common.process.common.GlrcTurnBasedEventListener
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent

class ZeventClipsSaverBotOnReaction(process: Process) : GlrcTurnBasedEventListener(process) {
    
    override fun onMessageReactionAdd(event: MessageReactionAddEvent) {
        event.retrieveMessage().queue {
            val channel = gateway.getTextChannelById(it.channel.idLong)!!
            
            if (channel.parentCategory!!.idLong == HardCodedValues.CATEGORY_EVENTS
                && channel.idLong != HardCodedValues.CHANNEL__ZEVENTS_CLIPZ
            ) {
                ZeventClipsSaverHelper.tryTo(gateway, it)
            }
        }
    }
}

