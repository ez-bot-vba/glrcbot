package com.bancarelvalentin.glrcbot.common.process.common.background.logdmbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class LogDmsBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__LOG_DMS__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__LOG_DMS__DESC)
    
    override fun getListeners(): Array<EventListener> {
        return arrayOf(LogDmsBotOnDm(this))
    }
}