package com.bancarelvalentin.glrcbot.common.process.common.background.deletetempchannelondisconnectbot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import net.dv8tion.jda.api.entities.VoiceChannel
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent
import java.time.Duration


class DeleteTempChannelOndisconnectBotOnDisconnect(process: Process) : EventListener(process) {
    
    override fun onGuildVoiceLeave(event: GuildVoiceLeaveEvent) {
        super.onGuildVoiceLeave(event)
        if (event.channelLeft.name.startsWith("-") && event.channelLeft.members.isEmpty()) {
            AsyncUtils.delay(Duration.ofMinutes(1), Runnable { event.channelLeft.delete().queue() })
        }
    }
    
    override fun onGuildVoiceJoin(event: GuildVoiceJoinEvent) {
        super.onGuildVoiceJoin(event)
        toJoinMap.remove(event.channelJoined.idLong)
    }
    
    companion object {
        val toJoinMap = ArrayList<Long>()
        
        fun onTmpGuildVoiceCreated(voiceChannel: VoiceChannel) {
            toJoinMap.add(voiceChannel.idLong)
            AsyncUtils.delay(Duration.ofMinutes(5), Runnable {
                if (toJoinMap.contains(voiceChannel.idLong)) {
                    voiceChannel.delete().queue()
                }
            })
        }
    }
}


