package com.bancarelvalentin.glrcbot.common.process.common.command.gameinvite

import com.bancarelvalentin.ezbot.process.command.param.StringCommandParam
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum

class MessageParam : StringCommandParam() {
    override val optional = true
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_INVITE__MESSAGE__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.COMMAND_DOC_CMD_PARAM__GAME_INVITE__MESSAGE__DESC)
}