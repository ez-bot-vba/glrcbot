package com.bancarelvalentin.glrcbot.common.process.common.background.mentionreplybot

import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.Process
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import net.dv8tion.jda.api.events.message.MessageReceivedEvent


class MentionReplyBotOnMessage(process: Process) : EventListener(process) {
    
    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (!event.message.author.isBot
            && event.message.referencedMessage == null
            && event.message.isMentioned(gateway.selfUser)
        ) {
            event.message.addReaction(FormatingUtils.getRandomEmojies(1)[0].tounicode()).queue()
        }
    }
}
