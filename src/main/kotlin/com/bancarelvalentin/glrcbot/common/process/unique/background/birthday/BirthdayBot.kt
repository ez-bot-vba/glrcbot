package com.bancarelvalentin.glrcbot.common.process.unique.background.birthday

import at.mukprojects.giphy4j.Giphy
import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.ezbot.process.EventListener
import com.bancarelvalentin.ezbot.process.background.BackgroundProcess
import com.bancarelvalentin.ezbot.utils.AsyncUtils
import com.bancarelvalentin.ezbot.utils.FormatingUtils
import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcLiveConfig
import com.bancarelvalentin.glrcbot.common.GlrcSimpleLocalizeEnum
import com.bancarelvalentin.glrcbot.common.HardCodedValues
import java.util.Calendar
import java.util.Random


class BirthdayBot : BackgroundProcess() {
    
    override val rawName = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__BIRTHDAY__NAME)
    override val rawDesc = CommonUtils.localize(GlrcSimpleLocalizeEnum.PROCESS_DOC__BIRTHDAY__DESC)
    
    override fun getListeners(): Array<EventListener> {
        Orchestrator.listenForLiveConfig(Runnable {
            (ConfigHandler.liveConfig as GlrcLiveConfig).birthdays
                .forEach {
                    val date = it.date
                    val instance = Calendar.getInstance()
                    instance.time = date
                    instance.set(Calendar.HOUR, 8 + Random().nextInt(12))
                    instance.set(Calendar.MINUTE, 1 + Random().nextInt(56))
                    AsyncUtils.schedule(instance.time, Runnable {
                        
                        val textChannelById = gateway.getTextChannelById(HardCodedValues.CHANNEL__GENERAL)!!
                        textChannelById
                            .sendMessage("Joyeux anniversaire ${FormatingUtils.formatUserId(it.id)} :partying_face:")
                            .queue()
                        
                        val results = Giphy((ConfigHandler.config as CommonConfig).giphyApiKey)
                            .search("happy birthday", 100, 0)
                            .dataList
                        
                        if (results.size > 0) {
                            val randomResultIndex = (0 until results.size).random()
                            textChannelById.sendMessage(results[randomResultIndex].images.original.url)
                        }
                    })
                }
        })
        return arrayOf()
    }
}
