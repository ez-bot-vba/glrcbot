package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.config.EnvConfig

@Suppress("MemberVisibilityCanBePrivate")
object HardCodedValues {
    
    // Values are for the "sndbox" guild if env mode is on; for the "GLRC" one otherwise
    
    // GUILDS
    val GUILD__GLRC = if (EnvConfig.DEV) 677258457778356237 else 665970861441941508L
    
    
    //Categories
    val CATEGORY_GAMES_TEXT_CHATS: Long = if (EnvConfig.DEV) 904465375146680320 else 665971303160741900
    val CATEGORY_GAMES_VOICE_CHATS: Long = if (EnvConfig.DEV) 904465410471108619 else 665970861441941512
    val CATEGORY_GAMES_PATCH_CHATS: Long = if (EnvConfig.DEV) 904465449532682380 else 897619363081883708
    val CATEGORY_ARCHIVES: Long = if (EnvConfig.DEV) 904465567426175067 else 904463106963537961
    val CATEGORY_EVENTS: Long = if (EnvConfig.DEV) 904567342594400257 else 904561628878610463
    
    // Channels
    val CHANNEL__BOT_COMMANDS_ADMIN: Long = if (EnvConfig.DEV) 954473565447651338 else 849707382892134430
    val CHANNEL__BOT_COMMANDS: Long = if (EnvConfig.DEV) 954473626311204904 else 667350953552248852
    val CHANNEL__ANNOUCEMENTS: Long = if (EnvConfig.DEV) 954473486494093313 else 682520409392021512
    val CHANNEL__ROLE_ASSIGNEMENT: Long = if (EnvConfig.DEV) 896519417066840124 else 667047714843787324
    val CHANNEL__INVITE: Long = if (EnvConfig.DEV) 904052939151265812 else 904093676815396925
    val CHANNEL__WELCOME: Long = if (EnvConfig.DEV) 954473486494093313 else 680568275717062689
    val CHANNEL__GENERAL: Long = if (EnvConfig.DEV) 954473486494093313 else 665970861441941514
    val CHANNEL__ZEVENTS_CLIPZ: Long = if (EnvConfig.DEV) 904567381110693928 else 904761231271948378
    
    // Roles
    val ROLE__CO_CAPITAINE: Long = if (EnvConfig.DEV) 955937726039994438 else 847971621922537512
    val ROLE__MAITRE_EQUIPAGE: Long = if (EnvConfig.DEV) 955937703868903424 else 667047504554229781
    
    // People
    val USER_VBA: Long = 443750643375800320
    
    val playerRoleSuffix = "player"
}
