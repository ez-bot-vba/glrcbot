package com.bancarelvalentin.glrcbot.common

import com.bancarelvalentin.ezbot.config.Config
import com.bancarelvalentin.ezbot.config.EnvConfig
import com.bancarelvalentin.ezbot.utils.ColorUtils
import net.dv8tion.jda.api.JDA
import java.awt.Color
import java.util.Locale


abstract class CommonConfig(@Suppress("unused") val identity: IDENTITY) : Config {
    
    // Main
    final override val versionNumber: String
        get() = CommonVersion().toString()
    
    @Suppress("unused")
    val commonPrefixes = arrayOf("!")
    override val language = Locale.FRANCE
    override val extraI18nProjectTokens = arrayOf("4aca5567ebcc46d38c7b15c0d11ada49")
    
    // Specific channels
    final override val errorLogsChannelId = 954473682581991454
    final override val lazyCommandsChannelId = 949051944553574510
    final override val liveConfigChannelId = 1009195339061927966
    override val disableLiveConfigEdit = true
    
    // Feature flags
    override val supportLazyCommands = false
    final override val supportDjPlayerViaCommands = true// Only one should support reaction player tho
    final override val logErrorsOnSentry = true
    final override val timeStartupInSentry = true
    final override val timeCommandExecutionsInSentry = true
    final override val timeCronExecutionsInSentry = true
    final override val createI18nOnBoot = EnvConfig.DEV
    
    // Access rights
    final override val moderatorsIds = arrayOf(HardCodedValues.USER_VBA)
    final override val moderatorsRolesIds = arrayOf(HardCodedValues.ROLE__CO_CAPITAINE)
    final override val defaultWhitelistedGuildIds = arrayOf(HardCodedValues.GUILD__GLRC)
    final override val defaultWhitelistedChannelIds = arrayOf(HardCodedValues.CHANNEL__BOT_COMMANDS, HardCodedValues.CHANNEL__BOT_COMMANDS_ADMIN)
    
    // Customizable values
    final override val defaultColor: Color
        get() = ColorUtils.hexToColor(defaultColorHex)
    
    // Not overrides
    open val defaultColorHex = "#ffffff"
    
    val giphyApiKey = System.getenv("GIPHY_API_KEY")
    
    @Suppress("MemberVisibilityCanBePrivate")
    val playerRoleColorHex = "#456af5"
    val playerRoleColor: Color
        get() = ColorUtils.hexToColor(playerRoleColorHex)
    val viewPermissionBit = 1024L//read access to the channel
    
    override val extraOnReadyListeners: Array<(JDA) -> Unit>
        get() = arrayOf(LogAllGuildsConnected())
    
    class LogAllGuildsConnected : (JDA) -> Unit {
        override fun invoke(p1: JDA) {
            val logger = com.bancarelvalentin.ezbot.logger.Logger(CommonConfig::class.java)
            p1.guilds.forEach { logger.info("Connected to guild #${it.id} (${it.name})") }
        }
    }
}

